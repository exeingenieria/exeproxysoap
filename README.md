## CGR
URL: /cgr/sistradoc/adjuntarArchivo
Metodo: POST
Parametros:
- servicio: String, corresponde al codigo de servicio. (ejemplo: 352 para hacienda)
- codRegion: String, corresponde al codigo de regi贸n. (ejemplo: 13 para Region Metropolitana)
- tipoDocCod: String, corresponde al codigo de tipo de documento. (ejemplo: 02 para decretos)
- folioDoc: String, corresponde al folio de un documento. no confundir con el folio del sobre.
- anno: String
- descripcion: String
- archivoUTF8B64: String, corresponde a una representaci贸n en base64 del archivo a adjuntar
- nombreArchivo: String
- mimeType: String, corresponde al mime type del archivo a adjuntar
- enviadoPor: String
Retorno:
- retorno: mensaje que devuelve WS de CGR

URL: /cgr/sistradoc/crearDocumento
Metodo: POST
Parametros:
- servicio: String, corresponde al codigo de servicio. (ejemplo: 352 para hacienda)
- documento: String, corresponde al archivo XML en texto plano a crear. No se debe envolver en CDATA.
Retorno:
- retorno: mensaje que devuelve WS de CGR

URL: /cgr/sistradoc/getDocumento
Metodo: POST
Parametros:
- servicio: String, corresponde al codigo de servicio. (ejemplo: 352 para hacienda)
- folio: String, corresponde al folio de un documento. no confundir con el folio del sobre.
- anno: String
- codRegion: String, corresponde al codigo de regi贸n. (ejemplo: 13 para Region Metropolitana)
- tipoDocCod: String, corresponde al codigo de tipo de documento. (ejemplo: 02 para decretos)   
Retorno:
- documento: Representaci髇 en base64 del documento solicitado
- oficio: Representaci髇 en base64 del oficio relacionado al documento
- error: mensaje que devuelve WS de CGR en caso de error

URL: /cgr/sistradoc/getEstadoDoc
Metodo: POST
Parametros:
- servicio: String, corresponde al codigo de servicio. (ejemplo: 352 para hacienda)
- folioDoc: String, corresponde al folio de un documento. no confundir con el folio del sobre.
- agno: String
- codRegion: String, corresponde al codigo de regi贸n. (ejemplo: 13 para Region Metropolitana)
- tipoDocCod: String, corresponde al codigo de tipo de documento. (ejemplo: 02 para decretos)   
Retorno:
- ok: mensaje que devuelve WS de CGR en caso de exito
- error: mensaje que devuelve WS de CGR en caso de error

## DIPRES
URL: /dipres/notificar
Metodo: POST
Parametros:
- expediente: String, corresponde al numero de expediente con formato exedoc (ejemplo: E513/2018)
- motivo: String, corresponde al motivo de la notificacion
- estadoTramitacion: String, corresponde al estado de tramitaci髇. (ejemplo: FIR, DEV)
- decretoNombre: String
- decretoData: String, corresponde a una representaci贸n en base64 de un decreto presupuestario.
- decretoContentType: String, corresponde al content type del decreto data.
- antecedenteNombre: String
- antecedenteMateria: String
- antecedenteData: String, corresponde a una representaci贸n en base64 del antecedente.
- antecedenteContentType: String, corresponde al content type del antecedente.
Retorno:
Objeto con dos parametros:
- codigo: codigo de respuesta del ws de dipres.
- mensaje: Mensaje de respuesta del ws de dipres.