package cgr.tomaRazon;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import cl.exe.interoperabilidad.cgr.sistradoc.AdjuntarArchivoRequest;
import cl.exe.interoperabilidad.cgr.sistradoc.AdjuntarArchivoResponse;
import cl.exe.interoperabilidad.cgr.sistradoc.TomadorDeRazon;
import exeProxySoap.ErrorServicio;

@Path("/cgr/sistradoc/adjuntarArchivo")
public class AdjuntarArchivo {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response adjuntarArchivo(String json) {
        Response res = null;
        Logger logger = Logger.getLogger(cgr.tomaRazon.AdjuntarArchivo.class.getName());
        
        logger.info("Inicio Adjuntar Archivo CGR");
                
		try {
	    	TomadorDeRazon tomador = new TomadorDeRazon();
	    	
	    	AdjuntarArchivoRequest req = new AdjuntarArchivoRequest();
	    	
	    	JSONObject jsonReq = new JSONObject(json);
	    	
	    	String servicio = null;
	    	String codRegion = null;
	    	String tipoDocCod = null;
	    	String folioDoc = null;
	    	String anno = null;
	    	String tipoAdj = null;
	    	String descripcion = null;
	    	String archivoUTF8B64 = null;
	    	String nombreArchivo = null;
	    	String mimeType = null;
	    	String enviadoPor = null;
	    	
	    	if (jsonReq.has("servicio")) {
	    		servicio = jsonReq.getString("servicio");
			}
	    	
	    	if (jsonReq.has("codRegion")) {
	    		codRegion = jsonReq.getString("codRegion");
			}
	    	
	    	if (jsonReq.has("tipoDocCod")) {
	    		tipoDocCod = jsonReq.getString("tipoDocCod");
			}
	    	
	    	if (jsonReq.has("folioDoc")) {
	    		folioDoc = jsonReq.getString("folioDoc");
			}
	    	
	    	if (jsonReq.has("anno")) {
	    		anno = jsonReq.getString("anno");
			}
	    	
	    	if (jsonReq.has("tipoAdj")) {
	    		tipoAdj = jsonReq.getString("tipoAdj");
			}
	    	
	    	if (jsonReq.has("descripcion")) {
	    		descripcion = jsonReq.getString("descripcion");
			}
	    	
	    	if (jsonReq.has("archivoUTF8B64")) {
	    		archivoUTF8B64 = jsonReq.getString("archivoUTF8B64");
			}
	    	
	    	if (jsonReq.has("nombreArchivo")) {
	    		nombreArchivo = jsonReq.getString("nombreArchivo");
			}
	    	
	    	if (jsonReq.has("mimeType")) {
	    		mimeType = jsonReq.getString("mimeType");
			}
	    	
	    	if (jsonReq.has("enviadoPor")) {
	    		enviadoPor = jsonReq.getString("enviadoPor");
			}
	    	
	    	if (servicio != null) {
	    		req.setServicio(servicio);
	    	}
	    	
	    	if (codRegion != null) {
	    		req.setCodRegion(codRegion);
	    	}
	    	
	    	if (tipoDocCod != null) {
	    		req.setTipoDocCod(tipoDocCod);
	    	}
	    	
	    	if (folioDoc != null) {
	    		req.setFolioDoc(folioDoc);
	    	}
	    	
	    	if (anno != null) {
	    		req.setAnno(anno);
	    	}
	    	
	    	if (tipoAdj != null) {
	    		req.setTipoAdj(tipoAdj);
	    	}
	    	
	    	if (descripcion != null) {
	    		req.setDescripcion(descripcion);
	    	}
	    	
	    	if (archivoUTF8B64 != null) {
	    		req.setArchivoUTF8B64(archivoUTF8B64);
	    	}
	    	
	    	if (nombreArchivo != null) {
	    		req.setNombreArchivo(nombreArchivo);
	    	}
	    	
	    	if (mimeType != null) {
	    		req.setMimeType(mimeType);
	    	}
	    	
	    	if (enviadoPor != null) {
	    		req.setEnviadoPor(enviadoPor);
	    	}
	
	    	AdjuntarArchivoResponse response = tomador.adjuntarArchivo(req);
			
			JSONObject jsonResp = new JSONObject();
			jsonResp.put("retorno", response.getAdjuntarArchivoReturn());
			logger.info("RETORNO: " + response.getAdjuntarArchivoReturn() );
			
			res = Response.status(Status.OK).entity(jsonResp.toString()).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			JSONObject jsonResp = new JSONObject();
			try {
				jsonResp.put("retorno", ErrorServicio.EXE_PROXY_SOAP_CGR_NO_DISPONIBLE);
				res = Response.status(Status.NOT_FOUND).entity(jsonResp.toString()).build();
				logger.info("RETORNO: " + ErrorServicio.EXE_PROXY_SOAP_CGR_NO_DISPONIBLE + ", El servicio adjuntarArchivo no esta disponible");
			} catch (JSONException e1) {
				e1.printStackTrace();
				res = Response.status(Status.INTERNAL_SERVER_ERROR).entity(null).build();
				logger.info("ERROR al armar la respuesta de error");
			}
		}
		
		logger.info("Fin Adjuntar Archivo CGR");
		return res;
	}
}
