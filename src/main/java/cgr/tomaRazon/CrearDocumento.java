package cgr.tomaRazon;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import cl.exe.interoperabilidad.cgr.sistradoc.CrearDocumentoRequest;
import cl.exe.interoperabilidad.cgr.sistradoc.CrearDocumentoResponse;
import cl.exe.interoperabilidad.cgr.sistradoc.TomadorDeRazon;
import exeProxySoap.ErrorServicio;

@Path("/cgr/sistradoc/crearDocumento")
public class CrearDocumento {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crearDocumento(String json) {
        Response res = null;
		
        Logger logger = Logger.getLogger(cgr.tomaRazon.CrearDocumento.class.getName());
        
        logger.info("Inicio Crear Documento CGR");
        
		try {
	    	TomadorDeRazon tomador = new TomadorDeRazon();
	    	
	    	CrearDocumentoRequest crearDocReq = new CrearDocumentoRequest();
	    	
	    	JSONObject jsonReq = new JSONObject(json);
	    	
	    	String servicio = null;
	    	String documento = null;
	    	
	    	if (jsonReq.has("servicio")) {
	    		servicio = jsonReq.getString("servicio");
			}
	    	
	    	if (jsonReq.has("documento")) {
	    		documento = jsonReq.getString("documento");
			}
	    	
	    	if (servicio != null) {
	    		crearDocReq.setServicio(servicio);
	    	}
	    	
	    	if (documento != null) {
	    		crearDocReq.setDocumento(documento);
	    	}
	    	
	    	CrearDocumentoResponse crearDocResp = tomador.crearDocumento(crearDocReq);
			
			JSONObject jsonResp = new JSONObject();
			jsonResp.put("retorno", crearDocResp.getCrearDocumentoReturn());
			logger.info("RETORNO: " + crearDocResp.getCrearDocumentoReturn() );
			
			res = Response.status(Status.OK).entity(jsonResp.toString()).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			JSONObject jsonResp = new JSONObject();
			try {
				jsonResp.put("retorno", ErrorServicio.EXE_PROXY_SOAP_CGR_NO_DISPONIBLE);
				res = Response.status(Status.NOT_FOUND).entity(jsonResp.toString()).build();
				logger.info("RETORNO: " + ErrorServicio.EXE_PROXY_SOAP_CGR_NO_DISPONIBLE + ", El servicio crearDocumento no esta disponible");
			} catch (JSONException e1) {
				e1.printStackTrace();
				res = Response.status(Status.INTERNAL_SERVER_ERROR).entity(null).build();
				logger.info("ERROR al armar la respuesta de error");
			}
		}
		
		logger.info("Fin Crear Documento CGR");
        return res;
	}
}
