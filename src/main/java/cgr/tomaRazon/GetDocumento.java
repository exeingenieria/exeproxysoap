package cgr.tomaRazon;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import cl.exe.interoperabilidad.cgr.sistradoc.GetDocumentoRequest;
import cl.exe.interoperabilidad.cgr.sistradoc.GetDocumentoResponse;
import cl.exe.interoperabilidad.cgr.sistradoc.TomadorDeRazon;
import exeProxySoap.ErrorServicio;

@Path("/cgr/sistradoc/getDocumento")
public class GetDocumento {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerDocumento(String json) {
        Response res = null;
		
        Logger logger = Logger.getLogger(cgr.tomaRazon.GetDocumento.class.getName());
        
        logger.info("Inicio Get Documento CGR");
        
		try {
	    	TomadorDeRazon tomador = new TomadorDeRazon();
	    	
	    	GetDocumentoRequest req = new GetDocumentoRequest();
	    	
	    	JSONObject jsonReq = new JSONObject(json);
	    	
	    	String servicio = null;
	    	String folio = null;
	    	String anno = null;
	    	String codRegion = null;
	    	String tipoDocCod = null;
	    	
	    	if (jsonReq.has("servicio")) {
	    		servicio = jsonReq.getString("servicio");
			}
	    	
	    	if (jsonReq.has("folio")) {
	    		folio = jsonReq.getString("folio");
			}
	    	
	    	if (jsonReq.has("anno")) {
	    		anno = jsonReq.getString("anno");
			}
	    	
	    	if (jsonReq.has("codRegion")) {
	    		codRegion = jsonReq.getString("codRegion");
			}
	    	
	    	if (jsonReq.has("tipoDocCod")) {
	    		tipoDocCod = jsonReq.getString("tipoDocCod");
			}
	    	
	    	if (servicio != null) {
	    		req.setServicio(servicio);
	    	}
	    	
	    	if (folio != null) {
	    		req.setFolio(folio);
	    	}
	    	
	    	if (anno != null) {
	    		req.setAnno(anno);
	    	}
	    	
	    	if (codRegion != null) {
	    		req.setCodRegion(codRegion);
	    	}
	    	
	    	if (tipoDocCod != null) {
	    		req.setTipoDocCod(tipoDocCod);
	    	}
	
	    	GetDocumentoResponse response = tomador.getDocumento(req);
			
			JSONObject jsonResp = new JSONObject();
			
			if (response.getGetDocumentoReturn() != null) {
				jsonResp.put("documento", response.getGetDocumentoReturn());
				logger.info("EXITO: lleva documento");
				if (response.getGetOficioReturn() != null) {
					jsonResp.put("oficio", response.getGetOficioReturn());
					logger.info("EXITO: lleva oficio");
				}
			} else {
				jsonResp.put("error", response.getError());
				logger.info("ERROR: " + response.getError() );
			}
			
			res = Response.status(Status.OK).entity(jsonResp.toString()).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			JSONObject jsonResp = new JSONObject();
			try {
				jsonResp.put("error", ErrorServicio.EXE_PROXY_SOAP_CGR_NO_DISPONIBLE);
				res = Response.status(Status.NOT_FOUND).entity(jsonResp.toString()).build();
				logger.info("ERROR: " + ErrorServicio.EXE_PROXY_SOAP_CGR_NO_DISPONIBLE + ", El servicio getDocumento no esta disponible");
			} catch (JSONException e1) {
				e1.printStackTrace();
				res = Response.status(Status.INTERNAL_SERVER_ERROR).entity(null).build();
				logger.info("ERROR al armar la respuesta de error");
			}
		}
		
		logger.info("Fin Get Documento CGR");
		return res;
	}
}
