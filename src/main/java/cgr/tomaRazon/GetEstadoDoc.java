package cgr.tomaRazon;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import cl.exe.interoperabilidad.cgr.sistradoc.GetEstadoDocRequest;
import cl.exe.interoperabilidad.cgr.sistradoc.GetEstadoDocResponse;
import cl.exe.interoperabilidad.cgr.sistradoc.TomadorDeRazon;
import exeProxySoap.ErrorServicio;

@Path("/cgr/sistradoc/getEstadoDoc")
public class GetEstadoDoc {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obtenerEstadoDocumento(String json) {
        Response res = null;
        
        Logger logger = Logger.getLogger(cgr.tomaRazon.GetEstadoDoc.class.getName());
        
        logger.info("Inicio Get Estado Documento CGR");
		
		try {
	    	TomadorDeRazon tomador = new TomadorDeRazon();
	    	
	    	GetEstadoDocRequest req = new GetEstadoDocRequest();
	    	
	    	JSONObject jsonReq = new JSONObject(json);
	    	
	    	String servicio = null;
	    	String folioDoc = null;
	    	String agno = null;
	    	String codRegion = null;
	    	String tipoDocCod = null;
	    	
	    	if (jsonReq.has("servicio")) {
	    		servicio = jsonReq.getString("servicio");
			}
	    	
	    	if (jsonReq.has("folioDoc")) {
	    		folioDoc = jsonReq.getString("folioDoc");
			}
	    	
	    	if (jsonReq.has("agno")) {
	    		agno = jsonReq.getString("agno");
			}
	    	
	    	if (jsonReq.has("codRegion")) {
	    		codRegion = jsonReq.getString("codRegion");
			}
	    	
	    	if (jsonReq.has("tipoDocCod")) {
	    		tipoDocCod = jsonReq.getString("tipoDocCod");
			}
	    	
	    	if (servicio != null) {
	    		req.setServicio(servicio);
	    	}
	    	
	    	if (folioDoc != null) {
	    		req.setFolioDoc(folioDoc);
	    	}
	    	
	    	if (agno != null) {
	    		req.setAgno(agno);
	    	}
	    	
	    	if (codRegion != null) {
	    		req.setCodRegion(codRegion);
	    	}
	    	
	    	if (tipoDocCod != null) {
	    		req.setTipoDocCod(tipoDocCod);
	    	}
	
	    	GetEstadoDocResponse response = tomador.getEstadoDoc(req);
			
			JSONObject jsonResp = new JSONObject();
			
			if (response.getGetEstadoDocReturn() != null) {
				jsonResp.put("ok", response.getGetEstadoDocReturn());
				logger.info("OK: " + response.getGetEstadoDocReturn() );
			} else {
				jsonResp.put("error", response.getError());
				logger.info("ERROR: " + response.getError() );
			}
			
			res = Response.status(Status.OK).entity(jsonResp.toString()).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			JSONObject jsonResp = new JSONObject();
			try {
				jsonResp.put("error", ErrorServicio.EXE_PROXY_SOAP_CGR_NO_DISPONIBLE);
				res = Response.status(Status.NOT_FOUND).entity(jsonResp.toString()).build();
				logger.info("ERROR: " + ErrorServicio.EXE_PROXY_SOAP_CGR_NO_DISPONIBLE + ", El servicio getEstado no esta disponible");
			} catch (JSONException e1) {
				e1.printStackTrace();
				res = Response.status(Status.INTERNAL_SERVER_ERROR).entity(null).build();
				logger.info("ERROR al armar la respuesta de error");
			}
		}
		
		logger.info("Fin Get Estado Documento CGR");
		return res;
	}
}
