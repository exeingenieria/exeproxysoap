package dipres;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import cl.exe.interoperabilidad.dipres.siapiop.Antecedente;
import cl.exe.interoperabilidad.dipres.siapiop.Decreto;
import cl.exe.interoperabilidad.dipres.siapiop.HaciendaNotificaADipres;
import cl.exe.interoperabilidad.dipres.siapiop.Notificador;
import cl.exe.interoperabilidad.dipres.siapiop.ResponseSiapiop;
import exeProxySoap.ErrorServicio;

@Path("/dipres/notificar")
public class Notificar {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response notificar(String json) {
        Response res = null;
        Logger logger = Logger.getLogger(dipres.Notificar.class.getName());
        
        logger.info("Inicio notificacion a Dipres");
        
        JSONObject jsonResp = new JSONObject();
        
		try {
			Decreto dec = new Decreto();
			Antecedente ant = new Antecedente();
			
			Notificador notificador = new Notificador();
			HaciendaNotificaADipres req = new HaciendaNotificaADipres();
			
			JSONObject jsonReq = new JSONObject(json);
			
			String decretoContentType = null;
			String decretoData = null;
			String decretoNombre = null;
			String antecedenteContentType = null;
			String antecedenteData = null;
			String antecedenteMateria = null;
			String antecedenteNombre = null;
			String estadoTramitacion = null;
			String motivo = null;
			String expediente = null;
						
			if (jsonReq.has("decretoContentType")) {
				decretoContentType = jsonReq.getString("decretoContentType");
			}
			
			if (jsonReq.has("decretoData")) {
				decretoData = jsonReq.getString("decretoData");
			}
			
			if (jsonReq.has("decretoNombre")) {
				decretoNombre = jsonReq.getString("decretoNombre");
			}
			
			if (jsonReq.has("antecedenteContentType")) {
				antecedenteContentType = jsonReq.getString("antecedenteContentType");
			}
			
			if (jsonReq.has("antecedenteData")) {
				antecedenteData = jsonReq.getString("antecedenteData");
			}
			
			if (jsonReq.has("antecedenteMateria")) {
				antecedenteMateria = jsonReq.getString("antecedenteMateria");
			}
			
			if (jsonReq.has("antecedenteNombre")) {
				antecedenteNombre = jsonReq.getString("antecedenteNombre");
			}
			
			if (jsonReq.has("estadoTramitacion")) {
				estadoTramitacion = jsonReq.getString("estadoTramitacion");
			}
			
			if (jsonReq.has("motivo")) {
				motivo = jsonReq.getString("motivo");
			}
			
			if (jsonReq.has("expediente")) {
				expediente = jsonReq.getString("expediente");
			}
			
			if (decretoContentType != null) {
				dec.setContentType(decretoContentType);
			}
			
			if (decretoData != null) {
				dec.setData(decretoData);
			}
			
			if (decretoNombre != null) {
				dec.setNombre(decretoNombre);
			}
			
			if (antecedenteContentType != null) {
				ant.setContentType(antecedenteContentType);
			}
			
			if (antecedenteData != null) {
				ant.setData(antecedenteData);
			}
			
			if (antecedenteMateria != null) {
				ant.setMateria(antecedenteMateria);
			}
			
			if (antecedenteNombre != null) {
				ant.setNombre(antecedenteNombre);
			}
			
			if (expediente != null) {
				req.setExpediente(expediente);
			}
			
			if (estadoTramitacion != null) {
				req.setEstadoTramitacion(estadoTramitacion);
			}
			
			if (motivo != null) {
				req.setMotivo(motivo);
			}
			
			if (expediente != null) {
				req.setExpediente(expediente);
			}
						
			String logParams = logParametros(expediente, estadoTramitacion, motivo, decretoNombre);
			
			logger.info("Parametros : \n" + logParams);
			
			req.setDecreto(dec);
			req.setAntecedente(ant);
			
			ResponseSiapiop response = notificador.notificar(req);
			
			if (response != null) {
				
				jsonResp.put("codigo", response.getCodigo());
				jsonResp.put("mensaje", response.getMsg());
				
				if ( ErrorServicio.EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE.equals( response.getCodigo() ) ){//2001
					
					jsonResp.put("status", Status.NOT_FOUND.getStatusCode());//404
					res = Response.status(Status.NOT_FOUND).entity(jsonResp.toString()).build();
					
				} else if ( ErrorServicio.EXE_PROXY_SOAP_NOTIFICACION_DIPRES_ERROR_GENERICO.equals( response.getCodigo() ) ) {//2003
					
					jsonResp.put("status", Status.INTERNAL_SERVER_ERROR.getStatusCode());//500
					res = Response.status(Status.INTERNAL_SERVER_ERROR).entity(jsonResp.toString()).build();
					
				} else {//200
					jsonResp.put("status", Status.OK.getStatusCode());
					res = Response.status(Status.OK).entity(jsonResp.toString()).build();
				}
			} else {
				jsonResp.put("codigo", ErrorServicio.EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE);
				jsonResp.put("mensaje", "El servicio de Notificación Dipres no está disponible");
				jsonResp.put("status", Status.NOT_FOUND.getStatusCode());
				res = Response.status(Status.NOT_FOUND).entity(jsonResp.toString()).build();
			}
			logger.info("Fin notificacion Dipres. Codigo: " + jsonResp.get("codigo") + " Mensaje: " + jsonResp.get("mensaje") );
			return res;
		} catch (Exception e) {
			//Aqui solo debiese llegar alguna exception asociada al procesamiento del json
			e.printStackTrace();
			
			try {
				jsonResp.put("codigo", ErrorServicio.EXE_PROXY_SOAP_NOTIFICACION_DIPRES_ERROR_GENERICO);
				jsonResp.put("mensaje", "Problemas al procesar los parámetros de entrada");
				jsonResp.put("status", Status.INTERNAL_SERVER_ERROR.getStatusCode());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			res = Response.status(Status.INTERNAL_SERVER_ERROR).entity(jsonResp.toString()).build();
			logger.info("Fin notificacion Dipres. Codigo: 2003 Mensaje: Problemas al procesar los parámetros de entrada");
		}
		return res;
	}
	
	private String logParametros(String expediente, String estadoTramitacion, String motivo, String decretoNombre) {
		
		String params = "Expediente : ";
		
		if (expediente != null) {
			params = params + expediente + "\n";
		} else {
			params = params + "\n";
		}
		
		if (estadoTramitacion != null) {
			params = params + "Estado: " + estadoTramitacion + "\n";
		} else {
			params = params + "\n";
		}
		
		if (motivo != null) {
			params = params + "Motivo: " + motivo + "\n";
		} else {
			params = params + "\n";
		}
		
		if (decretoNombre != null) {
			params = params + "Nombre decreto: " + decretoNombre + "\n";
		} else {
			params = params + "\n";
		}
		return params;
	}
}
