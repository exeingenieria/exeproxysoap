package exeProxySoap;


public class ErrorServicio {
	
	public static final String EXE_PROXY_SOAP_NOTIFICACION_DIPRES_EXE_PROXY_NO_DISPONIBLE = "2000";
	public static final String EXE_PROXY_SOAP_NOTIFICACION_DIPRES_NO_DISPONIBLE = "2001";
	public static final String EXE_PROXY_SOAP_CGR_NO_DISPONIBLE = "2002";
	public static final String EXE_PROXY_SOAP_NOTIFICACION_DIPRES_ERROR_GENERICO = "2003";
}
