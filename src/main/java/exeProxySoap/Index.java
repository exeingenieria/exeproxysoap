package exeProxySoap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jettison.json.JSONObject;


@Path("/exeProxySoap/index")
public class Index {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response sayHtmlHello() {
        Response res = null;
		
		try {
			JSONObject json = new JSONObject();
			json.put("codigo", "1");
			
			res = Response.status(Status.OK).entity(json.toString()).build();
			
			return res;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return res;
	}
}
